﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAPZLab5
{
    class Receiver
    {
        public bool isEnded { get; set; }
        public bool isFree { get; set; }
        public Receiver(bool isE, bool isF)
        {
            isEnded = isE;
            isFree = isF;
        }
    }
    abstract class TaskHandler
    {
        public TaskHandler successor { get; set; }
        public abstract void Handle(Receiver rec);
    }
    class DeadLineHandler : TaskHandler
    {
        public override void Handle(Receiver rec)
        {
            if (rec.isEnded == false)
                Console.WriteLine("NICE!\nDead line isn't come");
            else if (successor != null)
            {
                Console.WriteLine("Next handler, because you can't create a task in the past.");
                successor.Handle(rec);
            }
        }
    }
    class FreeDayHandler : TaskHandler
    {
        public override void Handle(Receiver rec)
        {
            if (rec.isFree == true)
                Console.WriteLine("NICE!\nDay is free");
            else if (successor != null)
            {
                Console.WriteLine("Next handler, because day isn't free");
                successor.Handle(rec);
            }
        }
    }
}
