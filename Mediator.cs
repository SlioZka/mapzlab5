﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAPZLab5
{
    abstract class Mediator
    {
        public abstract void Send(string msg, Components components);
    }
    abstract class Components
    {
        protected Mediator mediator;
        public Components(Mediator _mediator)
        {
            mediator = _mediator;
        }
        public virtual void Send(string message)
        {
            mediator.Send(message, this);
        }
        public abstract void Notify(string message);
    }
    class User : Components
    {
        public User(Mediator mediator) : base(mediator)
        {

        }
        public override void Notify(string message)
        {
            Console.WriteLine("Mess for User : " + message);
        }
    }
    class Prog : Components
    {
        public Prog(Mediator mediator) : base(mediator)
        {

        }
        public override void Notify(string message)
        {
            Console.WriteLine("Mess for Programm : " + message);
        }
    }
    class UIMediator : Mediator
    {
        public Components User { get; set; }
        public Components Prog { get; set; }
        public override void Send(string msg, Components components)
        {
            if (User == components)
                Prog.Notify(msg);
            else if (Prog == components)
                User.Notify(msg);
        }
    }
}
