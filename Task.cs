﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAPZLab5
{
    interface ITaskState
    {
        void End(Task task);
        void Start(Task task);
    }
    class Task
    {
        public ITaskState State { get; set; }
        public Task(ITaskState _state)
        {
            State = _state;
        }
        public void End()
        {
            State.End(this);
        }
        public void Start()
        {
            State.Start(this);
        }
    }
    class StartedTask : ITaskState
    {
        public void Start(Task task)
        {
            Console.WriteLine("Task is alredy started");
        }
        public void End(Task task)
        {
            Console.WriteLine("Ending task");
            task.State = new Endedtask();
        }
    }
    class Endedtask :ITaskState
    {
        public void Start(Task task)
        {
            Console.WriteLine("Start task again");
            task.State = new StartedTask();
        }
        public void End(Task task)
        {
            Console.WriteLine("Task is alredy ended");
        }
    }
}
